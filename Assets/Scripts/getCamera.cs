using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class getCamera : MonoBehaviour
{
    private WebCamTexture backCam;
    private bool camAvailable;
    private Texture defaultBackground;

    public RawImage background;
    public AspectRatioFitter fit;
    
    private void Start()
    {
        defaultBackground = background.texture;
        WebCamDevice[] devices = WebCamTexture.devices;

        if (devices.Length == 0)
        {
            Debug.Log("No camera detected!");
            camAvailable = false;
            return;
        }
/*      // actual function to guarantee back camera 
        for (int i = 0; i < devices.Length; i++)
        {   
            if (!devices[i].isFrontFacing)
            {
                backCam = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
            }

            if (backCam == null)
            {
                Debug.Log("Unable to find a back camera!");
                return;
            }
            
            backCam.Play();
            background.texture = backCam;

            camAvailable = true;
        }
        */
        // take first camera recognized for testing purposes
        backCam = new WebCamTexture(devices[0].name, Screen.width, Screen.height);
        backCam.Play();
        background.texture = backCam;

        camAvailable = true;
    }

    private void Update()
    {
        if (!camAvailable)
            return;
        
        float ratio = (float)backCam.width / (float)backCam.height;
        fit.aspectRatio = ratio;

        float scaleY = backCam.videoVerticallyMirrored ? -1f : 1f;
        background.rectTransform.localScale = new Vector3(1f, scaleY, 1f);

        int orient = -backCam.videoRotationAngle;
        background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
    }
}
