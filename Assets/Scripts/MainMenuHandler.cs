using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenuHandler : MonoBehaviour
{
    public GameObject MainMenu;
    public GameObject StatisticsMenu;

   



    void Awake()
    {
        
      

    }
    public void NewGame()
    {

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void openStatistics()
    {
        StatisticsMenu.SetActive(true);
        MainMenu.SetActive(false);

    }

    public void closeStatistics()
    {
        StatisticsMenu.SetActive(false);
        MainMenu.SetActive(true);
       
    }
    public void clearStatistics()
    {
        PlayerPrefs.DeleteAll();
       
    }
}
