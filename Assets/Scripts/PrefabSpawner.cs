using System.Collections;
using UnityEngine;

public class PrefabSpawner : MonoBehaviour
{

    private Vector3 center;
    private GameObject mainCamera;

    [SerializeField]
    private GameObject ghostPrefab;

    [SerializeField]
    private float spawnDelay;

    [SerializeField]
    private float distance;

    [SerializeField]
    private float speedIncrease;

    [SerializeField]
    private AudioClip spawnSound1;

    [SerializeField]
    private AudioClip spawnSound2;

    [SerializeField]
    private AudioClip spawnSound3;

    private void Start()
    {
        this.mainCamera = GameObject.Find("VIOCameraDevice");
        this.center = getPosition();
        Spawn();
    }


    public void Spawn()
    {
        StartCoroutine(waiter());
    }

    private void createGhost()
    {
        this.center = getPosition();
        Vector3 pos = RandomCircle(center, distance);
        Quaternion rot = Quaternion.FromToRotation(Vector3.forward, this.center - pos);
        // make ghost faster whenever a new one spawns
        ghostPrefab.GetComponent<MoveAtPlayer>().increaseSpeed(speedIncrease);
        playSpawnSound(pos);
        Instantiate(ghostPrefab, pos, rot);
    }

    private void playSpawnSound(Vector3 pos)
    {
        int random = Random.Range(1, 3);
        if (random == 1)
            AudioSource.PlayClipAtPoint(spawnSound1, pos);
        else if (random == 2)
            AudioSource.PlayClipAtPoint(spawnSound2, pos);
        else
            AudioSource.PlayClipAtPoint(spawnSound3, pos);
    }
    private Vector3 getPosition()
    {
        Vector3 position = this.mainCamera.transform.position;
        position.y -= 2.2f;
        return position;
    }

    Vector3 RandomCircle(Vector3 center, float radius)
    {
        float ang = Random.value * 360;
        Vector3 pos;
        pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = center.y;
        pos.z = center.z + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        return pos;
    }

    private IEnumerator waiter()
    {
        float spawnDelay = Random.value * 4f;
        yield return new WaitForSecondsRealtime(spawnDelay);
        createGhost();

    }
}
