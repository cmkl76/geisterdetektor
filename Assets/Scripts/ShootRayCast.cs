using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootRayCast : MonoBehaviour
{
    public Button button;
    public int ghostsKilled = 0;
    private PrefabSpawner spawner;

    private void Start()
    {
        button.onClick.AddListener(shootRayCast);
        spawner = GameObject.Find("ObjectSpawner").GetComponent<PrefabSpawner>();
    }
    private void Update()
    {
       
    }
    
    public void shootRayCast()
    {
        Debug.Log("test");
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 50));
        RaycastHit hitObject;
        if (Physics.Raycast(ray, out hitObject))
        {
            if (hitObject.transform.tag == "Ghost" && button)
            {
                Destroy(hitObject.transform.gameObject);
                ghostsKilled++;
                spawner.Spawn();
            }
            Debug.DrawLine(Camera.main.transform.position, Camera.main.transform.position + Camera.main.transform.forward * 20, Color.red);
        }
        
    }
}
   