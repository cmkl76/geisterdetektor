using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class UpdateScore : MonoBehaviour
{
    public float score;
    private float pointsIncreasePerSecond;
    public TextMeshProUGUI scoreText;
    private int updateInterval = 1;
    private CheckGameState checkGameStateScript;
    public GameObject IngameMenu;
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        pointsIncreasePerSecond = 1;
        checkGameStateScript = GetComponent<CheckGameState>();

    }

    // Update is called once per frame
    void Update()
    {
       if(checkGameStateScript.isGameOver != true) { 
            if (!IngameMenu.activeSelf)
            {
                score += pointsIncreasePerSecond * Time.deltaTime;
                int scoreInt = (int)score;
                if (Time.time >= updateInterval)
                {
                    updateInterval = Mathf.FloorToInt(Time.time) + 1;
                    scoreText.SetText("Score: " + scoreInt);
                }
            }
        
    }}
}
