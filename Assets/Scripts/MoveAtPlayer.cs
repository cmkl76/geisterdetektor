using UnityEngine;

public class MoveAtPlayer : MonoBehaviour
{
    private Transform target;
    private PrefabSpawner spawner;

    [SerializeField]
    private static float moveSpeed = 0.9f;

    private CheckGameState CheckGameStateScript;

    [SerializeField]
    private AudioClip dmgSound;

    [SerializeField]
    private float minDistance;

    public void increaseSpeed(float speed)
    {
        moveSpeed += speed;      
        Debug.Log("current speed: " + moveSpeed);
    }

    private void Start()
    {
        // get target and spawner
        target = GameObject.Find("VIOCameraDevice").transform;
        CheckGameStateScript = GameObject.Find("Canvas").GetComponent<CheckGameState>();
        spawner = GameObject.Find("ObjectSpawner").GetComponent<PrefabSpawner>();

        gameObject.GetComponent<Animator>().speed += 1.5f;
    }

    void Update()
    {
        if (target)
        {
            //Debug.Log(target.position);
            Vector3 targetPos = target.position;
            // adjustment so model fits and looks at player
            targetPos.y -= 2.2f;
            this.transform.LookAt(targetPos);
            transform.RotateAround(transform.position, transform.up, 180f);
            // don't move into the camera
            if ((transform.position - targetPos).magnitude > minDistance)
                transform.Translate(Vector3.back * Time.deltaTime * moveSpeed, Space.Self);
            else
            {
                // play hitsoud and trigger damage
                AudioSource.PlayClipAtPoint(dmgSound, transform.position);

                Debug.Log("Losing HP!");
                CheckGameStateScript.health--;
                spawner.Spawn();
                Destroy(gameObject);
            }
        }
    }
}
