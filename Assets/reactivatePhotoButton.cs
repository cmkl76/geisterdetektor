using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class reactivatePhotoButton : MonoBehaviour
{
    public Button button;
   
    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);

        button.interactable = true;
       

        // Code to execute after the delay
    }

    public void activateButton()
    {

        StartCoroutine(ExecuteAfterTime(2));
    }
}
