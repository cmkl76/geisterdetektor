using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class LoadStatistics : MonoBehaviour
{
    int totalGhostsKilled;
    int totalTimeSurvived;
    int longestTimeSurvived;

    public TextMeshProUGUI totalGhostsKilledText;
    public TextMeshProUGUI totalTimeSurvivedText;
    public TextMeshProUGUI longestTimeSurvivedText;


    void Awake()
    {
        totalGhostsKilled = PlayerPrefs.GetInt("totalGhostsKilled", 0);
        totalTimeSurvived = PlayerPrefs.GetInt("totalTimeSurvived", 0);
        longestTimeSurvived = PlayerPrefs.GetInt("longestTimeSurvived", 0);

        totalGhostsKilledText.SetText("Total Ghosts Eliminated: " + totalGhostsKilled);
        totalTimeSurvivedText.SetText("Total Time Survived: " + totalTimeSurvived + " s");
        longestTimeSurvivedText.SetText("Longest Time Survived: " + longestTimeSurvived + " s");
    }

}
