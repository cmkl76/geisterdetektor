using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CheckGameState : MonoBehaviour
{
    public bool isGameOver = false;
    private UpdateScore updateScoreScript;
    private ShootRayCast shootRayCastScript;
    public GameObject gameOverMenu;
    public TextMeshProUGUI finalScore;
    public TextMeshProUGUI finalGhostKilled;

    public GameObject cameraTrigger;
    public GameObject crosshair;
    public GameObject pauseButton;
    public GameObject scoreText;

    public int health = 5;
    // Start is called before the first frame update

    void Awake()
    {

        Time.timeScale = 1f;
    }

    void Start()
    {
        updateScoreScript = this.GetComponent<UpdateScore>();
        shootRayCastScript = Camera.main.GetComponent<ShootRayCast>();
    }

    // Update is called once per frame
    void Update()
    {
        if (health == 0)
        {
            isGameOver = true;
        }

        if (isGameOver)
        {
            finalScore.SetText("Total Time Survived: " + Mathf.FloorToInt(updateScoreScript.score) + " s");
            finalGhostKilled.SetText("Total Ghosts Killed: " + shootRayCastScript.ghostsKilled);
            gameOverMenu.SetActive(true);
            cameraTrigger.SetActive(false);
            crosshair.SetActive(false);
            pauseButton.SetActive(false);
            scoreText.SetActive(false);
            Time.timeScale = 0f;

            if (PlayerPrefs.HasKey("totalGhostsKilled"))
            {
                PlayerPrefs.SetInt("totalGhostsKilled", PlayerPrefs.GetInt("totalGhostsKilled") + shootRayCastScript.ghostsKilled);
            }
            else
            {
                PlayerPrefs.SetInt("totalGhostsKilled", shootRayCastScript.ghostsKilled);
            }

            if (PlayerPrefs.HasKey("totalTimeSurvived"))
            {
                PlayerPrefs.SetInt("totalTimeSurvived", PlayerPrefs.GetInt("totalTimeSurvived") + Mathf.FloorToInt(updateScoreScript.score));
            }

            else
            {
                PlayerPrefs.SetInt("totalTimeSurvived", Mathf.FloorToInt(updateScoreScript.score));
            }

            if (PlayerPrefs.HasKey("longestTimeSurvived"))
            {

                if (PlayerPrefs.GetInt("longestTimeSurvived") < Mathf.FloorToInt(updateScoreScript.score))
                {
                    PlayerPrefs.SetInt("longestTimeSurvived", Mathf.FloorToInt(updateScoreScript.score));
                }
            }
            else
            {
                PlayerPrefs.SetInt("longestTimeSurvived", Mathf.FloorToInt(updateScoreScript.score));
            }


            PlayerPrefs.Save();
            isGameOver = false;
            health = 1;
        }
    }

    public void setGameOver()
    {

        isGameOver = true;
    }
}
