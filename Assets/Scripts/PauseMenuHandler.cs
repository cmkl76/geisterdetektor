using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuHandler : MonoBehaviour
{
    public GameObject PauseMenu;
    public GameObject PauseButton;
    public GameObject CameraTrigger;
    public GameObject Crosshair;
    public GameObject scoreText;


    private void Start()
    {
        Crosshair.transform.position = new Vector3(Screen.width / 2, Screen.height / 2, 0);
    }
    public void  openPauseMenu()
    {

        PauseMenu.SetActive(true);
        PauseButton.SetActive(false);
        CameraTrigger.SetActive(false);
        Crosshair.SetActive(false);
        scoreText.SetActive(false);
        Time.timeScale = 0f;
    }

    public void closePauseMenu()
    {
        
        PauseMenu.SetActive(false);
        PauseButton.SetActive(true);
        CameraTrigger.SetActive(true);
        Crosshair.SetActive(true);
        scoreText.SetActive(true);
        Time.timeScale = 1f;


    }

   public void Retry()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
       
    }

    public void Quit()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
    // Start is called before the first frame update

}
